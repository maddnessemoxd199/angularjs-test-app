var webpack = require('webpack');
var path = require('path');
// var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: './src/app.js'
  },
  output: {
    filename: 'dist/[name].bundle.js'
  },

  devtool: 'source-map',

  plugins: [
    new ExtractTextPlugin('dist/bundle.css')
  ],

  module: {
    loaders: [

      {test: /\.js$/, loader: 'babel', exclude: /node_modules/ },

      { test: /\.html$/, loader: 'html?attrs=false' },

      // { test: /\.scss$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader') },

      // { test: /\.(jpe?g|png|gif)$/i, loader: 'url-loader?name=./img/[name].[ext]' },

      { test: /\.(woff|woff(2)?|eot|ttf|svg)$/, loader: 'file?name=dist/fonts/[name].[ext]' },

      { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader', { publicPath: './' }) }

    ]
  }
  // postcss: function () {
  //   return [autoprefixer({ browsers: ['last 2 versions'] })];
  // }
};
