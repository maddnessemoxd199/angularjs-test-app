import angular from 'angular';

import 'bootstrap/dist/css/bootstrap.css';
import './style.css';
import 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';

import Components from './components/index';

angular.module('App', ['ui.bootstrap', Components, uiRouter])
    .config(function ($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.when('', '/table');
      $urlRouterProvider.when('/', '/table');

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('table', {
            url: '/table',
            template: '<app-table></app-table>'
          })
          .state('tasks', {
            url: '/tasks/:id',
            template: '<task-details></task-details>'
          });
    });