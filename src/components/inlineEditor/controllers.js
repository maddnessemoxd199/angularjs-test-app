export default class MainInlineEditorController {
  constructor($http, $scope, $element, $timeout) {
    'ngInject';
    this.$http = $http;
    this.$scope = $scope;
    this.$element = $element;

    this.$scope.$watch('inlineEditorVisibility', (newValue, oldValue) => {
      if (newValue === oldValue) {
        return
      }
      $timeout(() => {this.$element[0].focus()});
    });
  }

  $onInit() {
    this.$scope.inlineEditorVisibility = false;
  }

  toggleInlineEditor() {
    this.$scope.inlineEditorVisibility = !this.$scope.inlineEditorVisibility;
  }

  updateTask(data) {
    this.$http({
      url: 'randomUrl',
      method: 'PUT',
      data: data
    })
  }

  changeDetailsName($event) {
    this.$scope.setDetailsName = $event.target.value;
    this.$scope.inlineEditorVisibility = false;
    this.updateTask(this.$scope.details);
  }
}