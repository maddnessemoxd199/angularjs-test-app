import template from './inlineEditor.html';
import controller from './controllers';

export default () => ({
  template,
  controller,
  restrict: 'E',
  replace: true,
  controllerAs: 'inlineEditorCtrl',
  bindToController: true
});