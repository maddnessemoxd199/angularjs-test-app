
import ng from 'angular';

import InlineEditorComponent from './components';

export default ng.module('app.components.inlineEditor', ['ui.bootstrap'])
    .directive('inlineEditor', InlineEditorComponent)
    .name;