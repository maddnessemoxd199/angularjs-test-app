import ng from 'angular';

import TaskComponent from './components';

export default ng.module('app.components.task', ['ui.bootstrap'])
    .directive('task', TaskComponent)
    .name;