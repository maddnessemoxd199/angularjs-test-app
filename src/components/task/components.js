import template from './task.html';
import controller from './controllers';

export default () => ({
  template,
  controller,
  restrict: 'A',
  replace: true,
  controllerAs: 'taskCtrl',
  bindToController: true
});