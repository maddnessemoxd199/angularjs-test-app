import angular from 'angular';

import AppTable from './appTable';
import Task from './task';
import TaskDetails from './taskDetails';
import InlineEditor from './inlineEditor'

export default angular.module('App.components', [AppTable, Task, TaskDetails, InlineEditor]).name;