export default class MainTaskDetailsController {
  constructor($http, $scope, $stateParams, $rootScope) {
    'ngInject';
    this.$http = $http;
    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
  }

  $onInit() {
    this.checkDB();
  }

  set setDetailsName(data) {
    this.$scope.details.name = data;
    this.$scope.detailsName = data;
  }

   setTask(data) {
     let tasks = data || this.$rootScope.tasks;
    this.$scope.id = this.$stateParams.id;
    this.$scope.details = tasks.filter((item) => {
      return +item.id === +this.$scope.id;
    })[0];
    this.$scope.detailsName = this.$scope.details.name;
  }

  toggleInlineEditor() {
    this.$scope.inlineEditorVisibility = !this.$scope.inlineEditorVisibility;
  }

  checkDB() {
    if(!this.$rootScope.tasks) {
      this.$http.get('/src/json/tasks.json').then((res) => {
        this.$rootScope.tasks = res.data;
        return this.$rootScope.tasks;
      }).then((data) => {
        this.setTask(data);
      });
    } else {
      this.setTask();
    }
  }
}