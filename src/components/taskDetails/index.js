
import ng from 'angular';

import TaskDetailsComponent from './components';

export default ng.module('app.components.taskDetails', ['ui.bootstrap'])
    .directive('taskDetails', TaskDetailsComponent)
    .name;