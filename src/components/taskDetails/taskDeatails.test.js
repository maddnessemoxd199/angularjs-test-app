import taskDetails from './index';
import 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';

describe('component: taskDetails', function() {
  var $componentController;
  var $rootScope;
  var $http;
  var $httpBackend;
  var $state;
  var $scope;
  var response;

  beforeEach(angular.mock.module(taskDetails));
  beforeEach(angular.mock.module(uiRouter));
  beforeEach(angular.mock.inject(function (_$componentController_, _$state_, _$rootScope_, _$http_, _$httpBackend_) {
    $componentController = _$componentController_;
    $rootScope = _$rootScope_;
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    $state = _$state_;
    $scope = $rootScope.$new();
    response =  $httpBackend.when('GET', '/src/json/tasks.json').respond(200, [{name: 'test1', 'id': 1},
      {name: 'test2', 'id': 2}]);
  }));

  it('should set task', function() {
    var ctrl = $componentController('taskDetails', {$scope: $scope, $state: $state, $rootScope: $rootScope});
    ctrl.$stateParams.id = 1;
    ctrl.$rootScope.tasks = [{name: 'test1', 'id': 1},{name: 'test2', 'id': 2}];
    ctrl.setTask();
    expect(ctrl.$scope.details.name).not.toBe(undefined);
    expect(ctrl.$scope.details.name).toEqual('test1');
  });

  it('should make request for a tasks collection', function() {
    var ctrl = $componentController('taskDetails', {$scope: $scope, $state: $state, $rootScope: $rootScope});
    ctrl.$stateParams.id = 1;
    ctrl.checkDB();
    $httpBackend.expectGET('/src/json/tasks.json');
    $httpBackend.flush();
    $rootScope.$apply();
    expect($rootScope.tasks).not.toBe(undefined);
    expect(ctrl.$scope.details.name).not.toBe(undefined);
    expect(ctrl.$scope.details.name).toEqual('test1');
  });

  it('should toggle inlineEditorVisibility prop', function() {
    var ctrl = $componentController('taskDetails', {$scope: $scope, $state: $state, $rootScope: $rootScope});
    ctrl.$scope.inlineEditorVisibility = false;
    ctrl.toggleInlineEditor();
    expect(ctrl.$scope.inlineEditorVisibility).toEqual(true);
  });



});