import template from './taskDetails.html';
import controller from './controllers';

export default () => ({
  template,
  controller,
  restrict: 'E',
  replace: true,
  controllerAs: 'taskDetailsCtrl',
  bindToController: true
});