// import './mainContent.css';

import ng from 'angular';

import TableComponent from './components';

export default ng.module('app.components.appTable', ['ui.bootstrap'])
    .directive('appTable', TableComponent)
    .name;