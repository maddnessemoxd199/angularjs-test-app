import template from './appTable.html';
import controller from './controllers';

export default () => ({
  template,
  controller,
  restrict: 'E',
  replace: true,
  controllerAs: 'tableCtrl',
  bindToController: true
});