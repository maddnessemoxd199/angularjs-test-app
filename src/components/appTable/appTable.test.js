import table from '../index';
import 'angular-ui-bootstrap';

describe('component: appTable', function() {
  var $componentController;
  var $rootScope;
  var $http;
  var $httpBackend;
  var response;

  beforeEach(angular.mock.module(table));
  beforeEach(angular.mock.inject(function (_$componentController_, _$rootScope_, _$http_, _$httpBackend_) {
    $componentController = _$componentController_;
    $rootScope = _$rootScope_;
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    response =  $httpBackend.when('GET', '/src/json/tasks.json').respond(200, {test: 'test'});
  }));

  it('should get tasks collection', function() {
    var ctrl = $componentController('appTable', {$rootScope: $rootScope});
    ctrl.getData();
    $httpBackend.expectGET('/src/json/tasks.json');
    $httpBackend.flush();
    $rootScope.$apply();
    expect($rootScope.tasks).not.toBe(undefined);
    expect(ctrl.$rootScope.tasks).toEqual({test: 'test'});
  });
});