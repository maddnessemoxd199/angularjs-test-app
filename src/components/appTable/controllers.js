export default class MainTableController {
  constructor($http, $scope, $rootScope) {
    'ngInject';
    this.$http = $http;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
  }

  $onInit() {
    this.getData();
  }

  getData () {
    this.$http.get('/src/json/tasks.json').then((res) => {
      this.$rootScope.tasks = res.data;
    });
  }
}